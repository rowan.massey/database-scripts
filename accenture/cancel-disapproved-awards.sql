declare
    type transaction_record_table is table of GG_SPEND.WAL_WALLET_TRANSACTION%ROWTYPE;

    transaction_records      transaction_record_table;
    transaction_record       GG_SPEND.WAL_WALLET_TRANSACTION%ROWTYPE;
    transaction_status       GG_SPEND.WAL_WALLET_TX_STATUS%ROWTYPE;
    new_transaction_group_id varchar2(255);
    transaction_wallet_id    varchar2(255);
    new_transaction_id       varchar2(255);
    record_count             number;
    cursor disapproved_nomination_cursor is
        select NG.NOMINATION_LINE_ITEM_ID
        from GG_USER.GG_PENDING_ORDERS GPO
                 join GG_USER.GG_NOMINATION_GROUP NG
                      on NG.FK_PENDING_ORDER = GPO.PK_PENDING_ORDER
        where GPO.CLIENT_STORECODE = 'INTCOR5780'
          and GPO.STATUS = 2
          and GPO.CREATED >= '01-MAY-2023';

begin
    DBMS_OUTPUT.PUT_LINE('Starting processing of the cursor.');

    for disapproved_nomination in disapproved_nomination_cursor
        loop
            begin
                DBMS_OUTPUT.PUT_LINE('Processing disapproved award for line item id: ' ||
                                     disapproved_nomination.NOMINATION_LINE_ITEM_ID);

                -- Get the transaction record for the line item id. The nomination line item is stored in the link record.
                -- This could result in multiple records if the transaction has already been cancelled.
                select wt.* bulk collect
                into transaction_records
                from GG_SPEND.WAL_WALLET_TRANSACTION wt
                         join GG_SPEND.WAL_WALLET_TX_LINK wtl
                              on wt.transaction_id = wtl.transaction_id
                where wtl.link = disapproved_nomination.NOMINATION_LINE_ITEM_ID;

                if transaction_records.count > 0 then
                    -- Need to pick the first record. We're only interested in the transaction id. 
                    -- The check for already cancelled transactions will happen later.
                    transaction_record := transaction_records(1);

                    if transaction_record.TRANSACTION_ID is not null then
                        DBMS_OUTPUT.PUT_LINE('Transaction has been found. Processing transaction id: ' ||
                                             transaction_record.TRANSACTION_ID);

                        select count(*)
                        into record_count
                        from GG_SPEND.WAL_WALLET_TX_LINK
                        where link_type = 'CXL'
                          and transaction_id = transaction_record.TRANSACTION_ID;

                        -- Ensure that the original transaction wasn't already cancelled.
                        if record_count = 0 then
                            -- Need to check to ensure that the transaction was committed.
                            select *
                            into transaction_status
                            from GG_SPEND.WAL_WALLET_TX_STATUS
                            where transaction_id = transaction_record.TRANSACTION_ID
                              and status = 'COMMITTED';

                            -- If committed, then a new cancel transaction needs to be created.
                            if transaction_status.STATUS = 'COMMITTED' then
                                DBMS_OUTPUT.PUT_LINE('Transaction was committed. Creating cancelled transaction record with required links.');

                                --Get the wallet id for the existing transactions group id.
                                select wallet_id
                                into transaction_wallet_id
                                from GG_SPEND.WAL_WALLET_TX_GROUP
                                where tx_group_id = transaction_record.TX_GROUP_ID;

                                --Get the new UUID values for the new group and transaction records.
                                select new_uuid(), new_uuid()
                                into new_transaction_group_id, new_transaction_id
                                from dual;

                                --DBMS_OUTPUT.PUT_LINE('Inserting cancelled group with id : ' || new_transaction_group_id);
                                --DBMS_OUTPUT.PUT_LINE('Inserting cancelled transaction with id : ' || new_transaction_id);

                                --Add in the new transaction group and transaction records.
                                insert into GG_SPEND.WAL_WALLET_TX_GROUP
                                values (new_transaction_group_id, sysdate,
                                        'An award you gave from your wallet was not approved for delivery, so the points-to-give balance has been returned to your wallet.',
                                        1, 'CXL_AWARD', transaction_wallet_id, null, null);

                                insert into GG_SPEND.WAL_WALLET_TRANSACTION
                                values (new_transaction_id, transaction_record.AMOUNT * -1, null,
                                        transaction_record.TRANSFER_ACCOUNT_ID, new_transaction_group_id, 0,
                                        null);

                                DBMS_OUTPUT.PUT_LINE('Inserted new group with id ' || new_transaction_group_id ||
                                                     ' and transaction with id ' || new_transaction_id);

                                -- Add in a CXL link record for the original transaction
                                insert into GG_SPEND.WAL_WALLET_TX_LINK
                                values (new_uuid(), new_transaction_id, 'CXL',
                                        transaction_record.TRANSACTION_ID);
                                DBMS_OUTPUT.PUT_LINE('Inserted cancelled transaction link record for original transaction record.');

                                -- And in an ORIG link record and link to the old transaction
                                insert into GG_SPEND.WAL_WALLET_TX_LINK
                                values (new_uuid(), transaction_record.TRANSACTION_ID, 'ORIG',
                                        new_transaction_id);
                                DBMS_OUTPUT.PUT_LINE('Inserted orig link record for new transaction to original transaction records.');

                                -- Copy the award links.
                                insert into GG_SPEND.WAL_WALLET_TX_LINK
                                select new_uuid(), link, link_type, new_transaction_id
                                from GG_SPEND.WAL_WALLET_TX_LINK
                                where transaction_id = transaction_record.TRANSACTION_ID
                                  and link_type = 'AWARD';
                                DBMS_OUTPUT.PUT_LINE('Inserted any award link records for the new cancelled transaction from the original transaction record.');

                            else
                                DBMS_OUTPUT.PUT_LINE('Transaction was NOT committed. Skipping processing of this transaction.');
                            end if;
                        else
                            DBMS_OUTPUT.PUT_LINE('Original transaction has already been cancelled for id: ' ||
                                                 transaction_record.TRANSACTION_ID);
                        end if;
                    end if;
                else
                    DBMS_OUTPUT.PUT_LINE('No wallet transaction records found with nomination line item : ' ||
                                         disapproved_nomination.NOMINATION_LINE_ITEM_ID);
                end if;
            exception
                when NO_DATA_FOUND then
                    DBMS_OUTPUT.PUT_LINE('No transaction found when processing award for line item id: ' ||
                                         disapproved_nomination.NOMINATION_LINE_ITEM_ID);
            end;
        end loop;
    DBMS_OUTPUT.PUT_LINE('Finished processing of the cursor.');
end;
/

commit;